package com.sourceit.kotlincoroutines

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.concurrent.schedule
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        runFunctionAfter2MinutesByHandler()
//        runFunctionAfter2MinutesByThread1()
//        runFunctionAfter2MinutesByThread2()
//        runFunctionAfter2MinutesByViewPost()
//        runFunctionAfter2MinutesByTimer()
//        runFunctionAfter2MinutesByCoroutine()
//
//        startActivity(Intent(this, CoroutineActivity::class.java))
    }

    private fun runFunctionAfter2MinutesByHandler() {
        Handler().postDelayed({ toast() }, 2000)
    }

    private fun runFunctionAfter2MinutesByThread1() {
        thread {
            Thread.sleep(2000L)
            runOnUiThread { toast() }
        }
    }

    private fun runFunctionAfter2MinutesByThread2() {
        thread {
            Thread.sleep(2000L)
            root.post { toast() }
        }
    }

    private fun runFunctionAfter2MinutesByViewPost() {
        root.postDelayed({ toast() }, 2000L)
    }

    private fun runFunctionAfter2MinutesByTimer() {
        Timer().schedule(2000) { runOnUiThread { toast() } }
    }

    private fun runFunctionAfter2MinutesByCoroutine() {
        GlobalScope.launch {
            delay(2000L)
            withContext(Dispatchers.Main) {
                toast()
            }
        }
    }

    private fun toast() {
        Toast.makeText(this, "Hello!", Toast.LENGTH_SHORT).show()
    }
}
